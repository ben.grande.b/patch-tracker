# patch-tracker

Tracking my personal patches and contributions to FOSS Projects.

## QubesOS
<!-- vim-markdown-toc Marked -->

<!-- vim-markdown-toc -->

| Code  | Type |
| :---: | --- |
| C     | Code contribution |
| P     | Patch (small contribution) |
| Q     | Question |

| Code      | Status |
| :---:     | --- |
| A         | Applied |
| E         | Ended, used for questions |
| WR[:name] | Waiting review, optional name |
| WT[:name] | Waiting third-party change to analogous code, optional name |

| Date | Subject | Type  | Status | Notes | Links |
| ---  | ---     | :---: | :---:  | ---   | ---   |
| 23-05-18 | qubes-policy-lint and qubes-policy-editor-terminal | C | A | | [initial](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05224.html), [resolution](https://github.com/QubesOS/qubes-core-qrexec/pull/122) |
| 23-05-18 | Fix missing include in RPC names in admin | P | A | | [initial](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05225.html), [resolution](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05226.html) |
| 23-05-24 | vim-qrexec | C | WR: Demi, Marek | | [initial](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05231.html), [pending](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05239.html) |
| 23-05-26 | Fix python3-qrexec missing on qubes-core-qrexec | P | A | | [initial](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05243.html), [resolution](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05244.html) |
| 23-05-26 | parser: Change warning of invalid path to error | P | A | | [initial](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05245.html), [resolution](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05293.html) |
| 23-05-26 | qrexec parser - !include-dir allows multiple params | Q | E | Fixed linter | [initial](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05246.html) |
| 23-05-27 | Fix policy.Replace changing the file mode | P | A |  | [initial](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05255.html), [resolution](https://www.mail-archive.com/qubes-devel@googlegroups.com/msg05292.html) |
